import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import * as Actions from '../actions';

import BookList from '../components/book/BookList';

class AuthorDetailPage extends Component {

  componentWillMount() {
    this.props.fetchAuthor(this.props.params.id);
  }

  deleteAuthor = () => {

    this.props.deleteAuthor(this.props.params.id)
      .then(() => {
        this.context.router.push('/');
      })
  }

  render() {
    return (
      <div className="container">
        <div className="card">
          <div className="card-header">
            <div className="d-flex align-items-center justify-content-between">
              <div>
                Sobre o autor
              </div>
              <button
                onClick={this.deleteAuthor}
                className="btn btn-danger btn-sm">
                Remover
              </button>
            </div>
          </div>
          <div className="card-block">
            <div className="d-flex align-items-center justify-content-between">
              <div>
                <h4 className="card-title">
                  { this.props.firstName } {this.props.lastName}
                </h4>
                <p className="card-text">Veja abaixo a lista de livros deste autor.</p>
              </div>
              <Link
                to={`/autor/edit/${this.props.id}`}
                className="btn btn-secondary">
                Alterar dados
              </Link>
            </div>
          </div>
        </div>

        <div className="mt-3">
          <Link
            to={`/livroAutor/${this.props.id}/novo`}
            className="btn btn-success mb-3">
            Novo Livro
          </Link>
          <BookList authorId={this.props.params.id} />
        </div>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  const { id, firstName, lastName } = state.author;

  return {
    id,
    firstName,
    lastName
  };
};

AuthorDetailPage.contextTypes = {
  router: React.PropTypes.object
};

export default connect(mapStateToProps, Actions)(AuthorDetailPage);
