import { ActionTypes } from '../constants/index';

const initialState = {
  books: []
};

export default function(state = initialState, action) {
  switch(action.type) {
    case ActionTypes.SET_AUTHOR_BOOKS:
      return {
        ...state,
        books: action.payload
      };
    case ActionTypes.BOOK_DELETE_SUCCESS:
      const bookId = action.payload;
      const books = state.books.filter(book => bookId !== book.id);

      return {
        ...state,
        books
      };
    default: return state;
  }
}
