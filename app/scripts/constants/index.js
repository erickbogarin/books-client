/**
 * @namespace Constants
 * @desc App constants
 */

/**
 * @constant {Object} ActionTypes
 * @memberof Constants
 */
export const ActionTypes = {
  FETCH_AUTHORS: 'FETCH_AUTHORS',
  SET_AUTHORS: 'SET_AUTHORS',
  SET_AUTHORS_COUNT: 'SET_AUTHORS_COUNT',

  SET_NEXT_PAGE: 'SET_NEXT_PAGE',
  'RESET_PAGINATION': 'RESET_PAGINATION',

  AUTHOR_UPDATE: 'AUTHOR_UPDATE',
  AUTHOR_SAVE_SUCCESS: 'AUTHOR_SAVE_SUCCESS',
  AUTHOR_UPDATE_SUCCESS: 'AUTHOR_UPDATE_SUCCESS',
  AUTHOR_DELETE_SUCCESS: 'AUTHOR_DELETE_SUCCESS',
  SET_AUTHOR: 'SET_AUTHOR',
  RESET_AUTHOR: 'RESET_AUTHOR',

  SET_AUTHOR_BOOKS: 'SET_AUTHOR_BOOKS',
  BOOK_SAVE_SUCCESS: 'BOOK_SAVE_SUCCESS',
  BOOK_DELETE_SUCCESS: 'BOOK_DELETE_SUCCESS'
};

/**
 * @constant {Object} Pagination
 * @memberof Constants
 */
export const PaginationConfig = {
  limit: 5
}

/**
 * @constant {Object} XHR
 * @memberof Constants
 */

let protocol = 'http';

if (process.env.NODE_ENV === 'production') {
  protocol = 'https';
}

export const XHR = {
  ROOT_URL: `${protocol}://libraryapp-alezio.rhcloud.com/api/`
};
