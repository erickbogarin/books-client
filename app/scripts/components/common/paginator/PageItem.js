import React from 'react';
import classnames from 'classnames';

const PageItem = props => {
  const { currentPage, handlePageChanges, label, pageNumber } = props;
  return(
    <li
      className={classnames('page-item', {active: currentPage === pageNumber})}>
      <a href="#" onClick={() => handlePageChanges(pageNumber)} className="page-link">
        { label }
      </a>
    </li>
  );
};

export default PageItem;
