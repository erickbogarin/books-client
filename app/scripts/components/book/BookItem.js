import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../../actions';

class BookItem extends Component {
  deleteBookHandler = () => {
    const { book, authorId } = this.props;

    this.props.bookDelete({
      authorId,
      bookId: book.id
    })
  }

  render() {
    const { book } = this.props;

    return (
      <tr>
        <th scope="row">{ book.id }</th>
        <td>{ book.title }</td>
        <td onClick={this.deleteBookHandler}>Remover</td>
      </tr>
    );
  }
}

export default connect(null, Actions)(BookItem);
