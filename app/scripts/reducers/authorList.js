import { ActionTypes } from '../constants/index';
import { PaginationConfig } from '../constants';

const initialState = {
  currentPage: 1,
  authorsCount: 0,
  offset: 0,
  authors: []
};

export default function(state = initialState, action) {
  switch(action.type) {
    case ActionTypes.SET_AUTHORS:
      return {
        ...state,
        authors: action.payload.authors,
        currentPage: action.payload.currentPage,
        offset: action.payload.offset
      };
    case ActionTypes.SET_AUTHORS_COUNT:
      return {
        ...state,
        authorsCount: action.payload.count
      }
    default: return state;
  }
}
