import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import * as Actions from '../../actions';

import ListErrors from '../common/ListErrors';

class AuthorForm extends Component {
  state = {
    errors: {},
  };

  handleSubmit = (e) => {
    e.preventDefault();

    // validation
    let errors = {};
    if (this.props.firstName === '') errors.firstName = 'O campo nome é obrigatório';
    if (this.props.lastName === '') errors.lastName = 'O campo sobrenome é obrigatório';

    this.setState({ errors });
    const isValid = Object.keys(errors).length === 0;

    if (isValid) {
      this.props.submitHandler();
    }
  }

  inputChangeHandler = (e) => {
    if (!!this.state.errors[e.target.name]) {
      let errors = Object.assign({}, this.state.errors);
      delete errors[e.target.name];
      this.setState({
        errors
      });
    }

    this.props.authorUpdate({
      prop: e.target.name,
      value: e.target.value
    });
  }

  render() {
    return (
      <div className="container">
        <ListErrors errors={this.state.errors} />
        <form onSubmit={this.handleSubmit}>
          <fieldset>
            <legend>Novo Autor</legend>
            <hr/>
            <div className="form-group">
              <label htmlFor="firstName">Nome</label>
              <input
                type="text"
                id="firstName"
                name="firstName"
                className="form-control"
                value={this.props.firstName}
                onChange={this.inputChangeHandler} />
            </div>
            <div className="form-group">
              <label htmlFor="lastName">Sobrenome</label>
              <input
                type="text"
                id="lastName"
                name="lastName"
                className="form-control"
                value={this.props.lastName}
                onChange={this.inputChangeHandler} />
            </div>
          </fieldset>
          <div className="d-flex justify-content-around">
            <button disabled={this.props.loading} className="btn btn-success">Salvar</button>
            <Link className="btn btn-secondary" to="/">Cancelar</Link>
          </div>
        </form>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  const { firstName, lastName} = state.author;
  return {
    firstName,
    lastName
  };
};

export default connect(mapStateToProps, Actions)(AuthorForm);
