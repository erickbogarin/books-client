import { ActionTypes } from '../constants';

const initialState = {
  id: '',
  firstName: '',
  lastName: ''
};

export default function(state = initialState, action) {
  switch(action.type) {
    case ActionTypes.SET_AUTHOR:
      return {
        ...state,
        id: action.payload.id,
        firstName: action.payload.firstName,
        lastName: action.payload.lastName
      };
    case ActionTypes.AUTHOR_UPDATE:
      return {
        ...state,
        [action.payload.prop]: action.payload.value
      };
    case ActionTypes.AUTHOR_UPDATE_SUCCESS:
      return {
        ...state,
        firstName: action.payload.firstName,
        lastName: action.payload.lastName
      };
    case ActionTypes.AUTHOR_DELETE_SUCCESS:
      return initialState;
    case ActionTypes.RESET_AUTHOR:
      return initialState;
    case ActionTypes.AUTHOR_SAVE_SUCCESS:
      return initialState;
    default: return state;
  }
}
