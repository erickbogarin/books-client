import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../../actions';

import AuthorForm from './AuthorForm';

class AuthorEdit extends Component {
  state = {
    loading: false
  };

  handleFormSubmit = () => {
    const { firstName, lastName } = this.props;
    const { id } = this.props.params;

    this.setState({
      loading: true
    });

    return this.props.authorSave({
      id,
      firstName,
      lastName
    }).then(res => {
      this.context.router.push(`/autor/${id}`);
    });
  }

  render() {
    return (
      <div>
        <AuthorForm
          loading={this.state.loading}
          submitHandler={this.handleFormSubmit} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  console.log(state.author);
  const { firstName, lastName } = state.author;

  return {
    firstName,
    lastName
  };
}

AuthorEdit.contextTypes = {
  router: React.PropTypes.object
};

export default connect(mapStateToProps, Actions)(AuthorEdit);
