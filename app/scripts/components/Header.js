import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import classnames from 'classnames';

const Header = () => {
  return(
    <nav className="navbar navbar-toggleable-md navbar-light bg-faded mb-5">
      <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <Link className="navbar-brand" to="/">Bookstore</Link>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="nav navbar-nav pull-xs-right">
          <li className="nav-item">
            <Link className="nav-link" to="/autor/novo">Novo Autor <span className="sr-only">(current)</span></Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default connect(null, null)(Header);
