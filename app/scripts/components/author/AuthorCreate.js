import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../../actions';
import NotificationSystem from 'react-notification-system';

import AuthorForm from './AuthorForm';

class AuthorCreate extends Component {
  state = {
    loading: false
  };

  constructor(props) {
    super(props);

    this.notificationSystem = null;
  }

  componentWillMount() {
    this.props.resetAuthor();
  }

  componentDidMount() {
    this.notificationSystem = this.refs.notificationSystem;
  }

  handleFormSubmit = () => {
    const { firstName, lastName } = this.props;

    this.setState({
      loading: true
    });

    this.props.authorCreate({ firstName, lastName })
      .then(() => {
        this.setState({
          loading: false
        });

        this.notificationSystem.addNotification({
          message: 'Autor adicionado com sucesso!',
          level: 'success'
        });
      });
  }

  render() {
    return (
      <div>
        <AuthorForm
          loading={this.state.loading}
          submitHandler={this.handleFormSubmit}
        />
        <NotificationSystem ref="notificationSystem" />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { firstName, lastName } = state.author;

  return {
    firstName,
    lastName
  };
}

export default connect(mapStateToProps, Actions)(AuthorCreate);
