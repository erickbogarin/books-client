import React from 'react';

const ListErrors = ({ errors }) => {
  if (Object.keys(errors).length !== 0 && errors.constructor === Object) {
    return (
      <ul className="bg-danger text-white p-3">
        {
          Object.keys(errors).map(key => {
            return (
              <li key={key}>
                {errors[key]}
              </li>
            );
          })
        }
      </ul>
    );
  } else {
    return null;
  }
}

export default ListErrors;
