import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../../actions';
import { Link } from 'react-router';
import NotificationSystem from 'react-notification-system';

import ListErrors from '../common/ListErrors';

class BookForm extends Component {
  state = {
    title: '',
    errors: {},
    loading: false
  };

  constructor(props) {
    super(props);

    this.notificationSystem = null;
  }

  componentDidMount() {
    this.notificationSystem = this.refs.notificationSystem;
  }

  handleSubmit = (e) => {
    e.preventDefault();

    if (this.isValidForm()) {
      this.setState({ loading: true });

      this.props.bookCreate({
        authorId : this.props.params.authorId,
        title: this.state.title
      }).then(() => {
        this.notificationSystem.addNotification({
          message: 'Livro adicionado com sucesso!',
          level: 'success'
        });

        this.setState({ loading: false, title: '' });
      });
    }
  }

  isValidForm() {
    let errors = {};
    if (this.state.title === '') errors.title = 'Título obrigatório';

    this.setState({ errors });

    return Object.keys(errors).length === 0;
  }

  inputChangeHandler = (e) => {
    if (!!this.state.errors[e.target.name]) {
      let errors = Object.assign({}, this.state.errors);
      delete errors[e.target.name];
      this.setState({
        errors
      });
    }

    this.setState({
      [e.target.name]: e.target.value
    });
  }

  render() {
    return (
      <div className="container">
        <ListErrors errors={this.state.errors} />

        <form onSubmit={this.handleSubmit}>
          <fieldset>
            <legend>Novo Livro</legend>
            <hr/>

            <div className="form-group">
              <label htmlFor="title">Título</label>
              <input
                type="text"
                className="form-control"
                placeholder="Título do livro"
                id="title"
                name="title"
                value={this.state.title}
                onChange={this.inputChangeHandler} />
            </div>
          </fieldset>

          <div className="d-flex justify-content-around">
            <button
              disabled={this.state.loading}
              className="btn btn-success">
              Salvar
            </button>
            <Link
              className="btn btn-secondary"
              to={`/autor/${this.props.params.authorId}`}>
              Cancelar
            </Link>
          </div>
        </form>
        <NotificationSystem ref="notificationSystem" />
      </div>
    );
  }
}

export default connect(null, Actions)(BookForm);
