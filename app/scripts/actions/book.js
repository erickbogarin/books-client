import axios from 'axios';

/**
 * @module Actions/Book
 * @desc Actions for Book
 */

import { ActionTypes, XHR } from '../constants';

/**
 * deleteAuthor
 *
 * @returns {Promise}
 */
export function bookDelete({ authorId, bookId }) {
  const url = `${XHR.ROOT_URL}/authors/${authorId}/books/${bookId}`;

  return dispatch => {
    return axios.delete(url)
      .then(() => {
        dispatch({
          type: ActionTypes.BOOK_DELETE_SUCCESS,
          payload: bookId
        });
      });
  };
}

/**
 * deleteAuthor
 *
 * @returns {Promise}
 */
export const bookCreate = ({ authorId, title }) => {

  const url = `${XHR.ROOT_URL}/authors/${authorId}/books`;

  return dispatch => {
    return axios.post(url, {
      authorId,
      title
    }).then(res =>
      dispatch({
        type: ActionTypes.BOOK_SAVE_SUCCESS,
        payload: res.data
      })
    );
  }
};
