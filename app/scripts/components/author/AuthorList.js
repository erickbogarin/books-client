import React, { Component } from 'react';

class AuthorList extends Component {
  renderList = (authors) => {
    if (authors.length) {
      return (
        <table className="table table-hover table-linkable">
          <thead className="thead-inverse">
            <tr>
              <th>#</th>
              <th>First Name</th>
              <th>Last Name</th>
            </tr>
          </thead>
          <tbody className="table-body">
            {
              authors.map(author => (
                <tr
                  key={author.id}
                  onClick={() =>  this.context.router.push(`/autor/${author.id}`)}
                  className="linkable">
                  <th scope="row">{ author.id }</th>
                  <td>{ author.firstName }</td>
                  <td>{ author.lastName }</td>
                </tr>
              )
            )}
          </tbody>
        </table>
      );
    }

    return (<div>Empty List</div>);
  };

  render() {
    return (
      <div>
        { this.renderList(this.props.authors) }
      </div>
    );
  }
};

AuthorList.contextTypes = {
  router: React.PropTypes.object
};

export default AuthorList;
