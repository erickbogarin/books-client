import { ActionTypes } from '../constants';

const initialState = {
  id: '',
  title: ''
};

export default function(state = initialState, action) {
  switch(action.type) {
    case ActionTypes.BOOK_SAVE_SUCCESS:
      return {
        ...state,
        id: action.payload.id,
        title: action.payload.title
      };
    default: return state;
  }
}
