import axios from 'axios';
import { ActionTypes, XHR } from '../constants';

/**
 * @module Actions/AuthorForm
 * @desc Actions for Author Form
 */

export const authorUpdate = ({ prop, value }) => {
  return {
    type: ActionTypes.AUTHOR_UPDATE,
    payload: { prop, value }
  };
};

/**
 * authorSave
 *
 * @returns {Promise}
 */
export function authorSave({ id, firstName, lastName }) {
  const url = `${XHR.ROOT_URL}/authors/${id}`;

  return dispatch => {
    return axios.put(url, {
      firstName,
      lastName
    }).then((res) => {
      dispatch({
        type: ActionTypes.AUTHOR_UPDATE_SUCCESS,
        payload: res.data
      });
    });
  };
}

export const authorCreate = ({ firstName, lastName }) => {

  const url = `${XHR.ROOT_URL}/authors`;

  return dispatch => {
    return axios.post(url, {
      firstName,
      lastName
    }).then(res =>
      dispatch({
        type: ActionTypes.AUTHOR_SAVE_SUCCESS,
        payload: res.data
      })
    );
  }
};
