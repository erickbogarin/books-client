import axios from 'axios';

/**
 * @module Actions/BookList
 * @desc Actions for Book List
 */

import { ActionTypes, XHR } from '../constants';

/**
 * setBooks
 *
 * @returns {Array}
 */
export function setAuthorBooks(books) {
  return {
    type: ActionTypes.SET_AUTHOR_BOOKS,
    payload: books
  };
}

/**
 * fetchAuthorBooks
 *
 * @returns {Promise}
 */
export function fetchAuthorBooks(authorId) {
  const url = `${XHR.ROOT_URL}/authors/${authorId}/books`;

  return dispatch => {
    return axios.get(url).then(res => {
      dispatch(setAuthorBooks(res.data));
    });
  };
}
