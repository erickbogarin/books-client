import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../../actions';

import BookItem from './BookItem';

class BookList extends Component {
  componentDidMount() {
    this.props.fetchAuthorBooks(this.props.authorId);
  }

  render() {
    const { books, authorId } = this.props;

    if (books.length) {
      return (
        <table className="table">
          <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
            </tr>
          </thead>
          <tbody>
            {
              books.map(book => (
                <BookItem key={book.id} book={book} authorId={authorId} />
              )
            )}
          </tbody>
        </table>
      );
    }

    return (<div>Empty List</div>);
  }
};

const mapStateToProps = (state) => {
  const { books } = state.bookList;

  return { books };
}

export default connect(mapStateToProps, Actions)(BookList);
