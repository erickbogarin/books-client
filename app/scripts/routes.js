import React from 'react'
import { Route, IndexRoute } from 'react-router'

import App from './containers/App'
import AuthorPage from './containers/AuthorPage';
import AuthorCreate from './components/author/AuthorCreate';
import AuthorEdit from './components/author/AuthorEdit';
import AuthorDetailsPage from './containers/AuthorDetailsPage';
import BookForm from './components/book/BookForm';

export default <Route path="/" component={App}>
  <IndexRoute component={AuthorPage} />
  <Route path="autor/novo" component={AuthorCreate} />
  <Route path="autor/:id" component={AuthorDetailsPage} />
  <Route path="autor/edit/:id" component={AuthorEdit} />
  <Route path="livroAutor/:authorId/novo" component={BookForm} />
</Route>
