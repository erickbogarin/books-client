import { combineReducers } from 'redux';

import authorList from './authorList';
import author from './author';
import bookList from './bookList';
import book from './book';

const reducers = combineReducers({
  authorList,
  author,
  bookList,
  book
});

export default reducers;
