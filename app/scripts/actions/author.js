import axios from 'axios';

/**
 * @module Actions/AuthorList
 * @desc Actions for Author List
 */

import { ActionTypes, XHR } from '../constants/index';

/**
 * setAuthor
 *
 * @returns {Array}
 */
export function setAuthor(author) {
  return {
    type: ActionTypes.SET_AUTHOR,
    payload: author
  };
}

/**
 * deleteAuthor
 *
 * @returns {Promise}
 */
export function deleteAuthor(id) {
  const url = `${XHR.ROOT_URL}/authors/${id}`;

  return dispatch => {
    return axios.delete(url)
      .then(() => {
        dispatch({
          type: ActionTypes.AUTHOR_DELETE_SUCCESS,
        });
      });
  };
}

/**
 * resetAuthor
 *
 * @returns {void}
 */
export function resetAuthor(id) {
  return {
    type: ActionTypes.RESET_AUTHOR
  }
}

/**
 * fetchAuthor
 *
 * @returns {Promise}
 */
export function fetchAuthor(id) {
  const url = `${XHR.ROOT_URL}/authors/${id}`;

  return dispatch => {
    return axios.get(url).then(res => {
      dispatch(setAuthor(res.data));
    });
  };
}

