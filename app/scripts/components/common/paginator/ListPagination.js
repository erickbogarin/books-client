import React, {Component} from 'react';

class ListPaginator extends Component {
  render() {
    return(
      <nav aria-label="Page navigation">
        <ul className="pagination">
          {this.props.children}
        </ul>
      </nav>
    );
  }
};

export default ListPaginator;
