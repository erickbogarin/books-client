import axios from 'axios';

/**
 * @module Actions/AuthorList
 * @desc Actions for Author List
 */

import { ActionTypes, PaginationConfig, XHR } from '../constants/index';

/**
 * setMovies
 *
 * @returns {Array}
 */
export function setAuthors(authors, offset, currentePage) {
  return {
    type: ActionTypes.SET_AUTHORS,
    payload: {
      authors,
      currentePage,
      offset
    }
  };
}

/**
 * fetchAuthorsCount
 *
 * @returns {Promise}
 */
export function fetchAuthorsCount() {
  const url = `${XHR.ROOT_URL}/authors/count`;

  return dispatch => {
    return axios.get(url).then(res => {
      dispatch({
        type: ActionTypes.SET_AUTHORS_COUNT,
        payload: res.data
      });
    });
  };
}


/**
 * fetchAuthors
 *
 * @returns {Promise}
 */
export function fetchAuthors({ order, offset, currentePage }) {
  let filter = {
    limit: PaginationConfig.limit,
    order,
    offset
  };
  filter = JSON.stringify(filter);

  const url = `${XHR.ROOT_URL}/authors?filter=${filter}`;

  return dispatch => {
    return axios.get(url).then(res => {
      dispatch(setAuthors(res.data, offset, currentePage));
    });
  };
}
