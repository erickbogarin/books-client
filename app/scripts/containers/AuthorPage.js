import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import classnames from 'classnames';
import * as Actions from '../actions';
import { PaginationConfig } from '../constants';

import AuthorList from '../components/author/AuthorList';
import Paginator from '../components/common/paginator';

class AuthorPage extends Component {
  state = {
    showDropdown: false,
    order: ''
  }

  componentDidMount() {
    this.props.fetchAuthors({
      order: '',
      offset: 0,
      currentPage: 1
    });
    this.props.fetchAuthorsCount();
  }

  sortingClickHandler = (e) => {
    this.setState({ order: e.target.name });

    this.props.fetchAuthors({
      order: e.target.name,
      offset: 0,
      currentPage: 1
    });

    this.setState({ showDropdown: false });
  }

  handlePageChanges = (page) => {

    if (page === 1) {
      this.props.fetchAuthors({
        order: this.state.order,
        offset: 0,
        currentPage: page
      });
    } else {
      const offset = ( PaginationConfig.limit ) * (page - 1);

      this.props.fetchAuthors({
        order: this.state.order,
        currentPage: page,
        offset,
      });
    }
  }

  render() {
    return(
      <div className="container">
        <h2>Nossos Autores</h2>
        <hr/>

        <div className="row">
          <div className="col-md-12 mb-3">
            <div className={ classnames('dropdown', { show: this.state.showDropdown}) }>
              <button
                onClick={() => this.setState({ showDropdown: !this.state.showDropdown })}
                className="btn btn-secondary dropdown-toggle"
                type="button"
                aria-haspopup="true"
                aria-expanded="false">
                Ordenar
              </button>
              <div className="dropdown-menu">
                <button
                  onClick={this.sortingClickHandler}
                  name="firstName"
                  className="dropdown-item"
                  type="button">
                  Nome
                </button>
                <button
                  onClick={this.sortingClickHandler}
                  name="lastName"
                  className="dropdown-item"
                  type="button">
                  Sobrenome
                </button>
              </div>
            </div>
          </div>
        </div>

        <AuthorList authors={this.props.authors} />
        <div className="d-flex justify-content-center">
          <Paginator
            authorsCount={this.props.authorsCount}
            currentPage={this.props.currentPage}
            handlePageChanges={this.handlePageChanges} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  authors: state.authorList.authors,
  offset: state.authorList.offset,
  authorsCount: state.authorList.authorsCount
});

export default connect(mapStateToProps, Actions)(AuthorPage);
